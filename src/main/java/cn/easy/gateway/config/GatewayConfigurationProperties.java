package cn.easy.gateway.config;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

/**
 * 配置
 *
 * @author zwb
 * @create 2020-11-06 21:16
 **/
@Setter
@Getter(AccessLevel.PUBLIC)
@ConfigurationProperties(prefix = "gateway.routes")
public class GatewayConfigurationProperties {

    private List<GatewayRouterConfigurationProperties> routerConfigurationPropertiesList;
}

package cn.easy.gateway;

import cn.easy.gateway.config.GatewayConfigurationProperties;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Gateway自动配置
 *
 * @author zwb
 * @create 2020-11-06 21:33
 **/
@RequiredArgsConstructor
@Component
@ConditionalOnProperty(prefix = "gateway",name = "enabled",havingValue = "true")
@EnableConfigurationProperties({GatewayConfigurationProperties.class})
public class GatewayAutoConfiguration {
    private final GatewayConfigurationProperties gatewayConfigurationProperties;

}
